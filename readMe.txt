The application must start with a Maven command: mvn spring-boot:run
It has a embedded in-memory database like H2.
The database and tables creation it's done by the application.

To run the automatic test the Maven command is: mvn test


Pre requirements

-JDK 8
-Maven