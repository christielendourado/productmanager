package com.christielen;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.christielen.domain.Image;
import com.christielen.domain.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.DEFINED_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductManagerApplicationTests {
	
	@Test
	public void contextLoads() {
	}
	
	@LocalServerPort
    private int port;

    private URI uri;
    private Client client;

    @Before
    public void setUp() throws Exception {
        this.uri = new URI("http://localhost:" + port);
        client = ClientBuilder.newClient();
    }
    
    @Test
    public void testA1() {
    	Product product = new Product();
    	product.setName("Gamin 1");
    	product.setDescription("Clock");
    	
        Response response = client.target(uri).path("product-manager-app").path("products").path("add")
                                  .request(MediaType.APPLICATION_JSON)
                                  .post(Entity.entity(product, MediaType.APPLICATION_JSON));

       assertEquals("Created", response.getStatusInfo().getReasonPhrase());
    }
    
    @Test
    public void testA2() {
    	Product product = new Product();
    	product.setName("Gamin 2");
    	product.setDescription("Clock 2");
    	
        Response response = client.target(uri).path("product-manager-app").path("products").path("add")
                                  .request(MediaType.APPLICATION_JSON)
                                  .post(Entity.entity(product, MediaType.APPLICATION_JSON));

       assertEquals("Created", response.getStatusInfo().getReasonPhrase());
    }
    
    @Test
    public void testA3() {
    	Product product = new Product();
    	product.setName("Gamin 3");
    	product.setDescription("Clock 3");
    	
        Response response = client.target(uri).path("product-manager-app").path("products").path("add")
                                  .request(MediaType.APPLICATION_JSON)
                                  .post(Entity.entity(product, MediaType.APPLICATION_JSON));

       assertEquals("Created", response.getStatusInfo().getReasonPhrase());
    }

    @SuppressWarnings("unchecked")
	@Test
    public void testA4() {
        Response response = client.target(uri).path("product-manager-app").path("products").path("details")
                                  .request(MediaType.APPLICATION_JSON).get();

       List<Product> produtos = response.readEntity(List.class);
       
       assertEquals(3, produtos.size());
    }
    
    @Test
    public void testA5() {
    	Product product = new Product();
    	product.setId(1);
    	product.setName("Gamin 1");
    	product.setDescription("Clock 1");
    	
        Response response = client.target(uri).path("product-manager-app").path("products").path("update")
                                  .request(MediaType.APPLICATION_JSON)
                                  .put(Entity.entity(product, MediaType.APPLICATION_JSON));

       assertEquals("Created", response.getStatusInfo().getReasonPhrase());
    }
    
    @Test
    public void testA6() {
    	Response response = client.target(uri).path("product-manager-app").path("products").path("delete").path("3")
                                  .request(MediaType.APPLICATION_JSON)
                                  .delete();

       assertEquals("No Content", response.getStatusInfo().getReasonPhrase());
    }
    
    @SuppressWarnings("unchecked")
	@Test
    public void testA7() {
        Response response = client.target(uri).path("product-manager-app").path("products").path("details")
                                  .request(MediaType.APPLICATION_JSON).get();

        List<Product> produtos = response.readEntity(List.class);
       
       assertEquals(2, produtos.size());
    }
    
    @Test
    public void testB1() {
    	Image image = new Image();
    	image.setType("png");
    	
        Response response = client.target(uri).path("product-manager-app").path("images").path("add")
                                  .request(MediaType.APPLICATION_JSON)
                                  .post(Entity.entity(image, MediaType.APPLICATION_JSON));

       assertEquals("Created", response.getStatusInfo().getReasonPhrase());
    }
    
    @Test
    public void testB2() {
    	Image image = new Image();
    	image.setType("jpeg");
    	
        Response response = client.target(uri).path("product-manager-app").path("images").path("add")
                                  .request(MediaType.APPLICATION_JSON)
                                  .post(Entity.entity(image, MediaType.APPLICATION_JSON));

       assertEquals("Created", response.getStatusInfo().getReasonPhrase());
    }
    
    @Test
    public void testB3() {
    	Image image = new Image();
    	image.setType("bmp");
    	
        Response response = client.target(uri).path("product-manager-app").path("images").path("add")
                                  .request(MediaType.APPLICATION_JSON)
                                  .post(Entity.entity(image, MediaType.APPLICATION_JSON));

       assertEquals("Created", response.getStatusInfo().getReasonPhrase());
    }
    
    @SuppressWarnings("unchecked")
	@Test
    public void testB4() {
        Client client = ClientBuilder.newClient();
        Response response = client.target(uri).path("product-manager-app").path("images").path("details")
                                  .request(MediaType.APPLICATION_JSON).get();

       List<Image> images = response.readEntity(List.class);
       
       assertEquals(3, images.size());
    }
    
    @Test
    public void testB5() {
    	Image image = new Image();
    	image.setId(1);
    	image.setType("gif");
    	
        Client client = ClientBuilder.newClient();
        Response response = client.target(uri).path("product-manager-app").path("images").path("update")
                                  .request(MediaType.APPLICATION_JSON)
                                  .put(Entity.entity(image, MediaType.APPLICATION_JSON));

       assertEquals("Created", response.getStatusInfo().getReasonPhrase());
    }
    
   @Test
    public void testB6() {
    	Client client = ClientBuilder.newClient();
        Response response = client.target(uri).path("product-manager-app").path("images").path("delete").path("3")
                                  .request(MediaType.APPLICATION_JSON)
                                  .delete();

       assertEquals("No Content", response.getStatusInfo().getReasonPhrase());
    }
    
    @SuppressWarnings("unchecked")
	@Test
    public void testB7() {
        Client client = ClientBuilder.newClient();
        Response response = client.target(uri).path("product-manager-app").path("images").path("details")
                                  .request(MediaType.APPLICATION_JSON).get();

       List<Image> images = response.readEntity(List.class);
       
       assertEquals(2, images.size());
    }
    
    @Test
    public void testC1() {
    	List<Image> images = new ArrayList<>();
    	
    	Image image = new Image();
    	image.setId(1);
    	
    	images.add(image);
    	
        Response response = client.target(uri).path("product-manager-app").path("images").path("addImage").path("1")
                                  .request(MediaType.APPLICATION_JSON)
                                  .post(Entity.entity(images, MediaType.APPLICATION_JSON));

       assertEquals("Created", response.getStatusInfo().getReasonPhrase());
    }
    
    @Test
    public void testC2() {
    	 Client client = ClientBuilder.newClient();
         Response response = client.target(uri).path("product-manager-app").path("images").path("detailsImages").path("1")
                                   .request(MediaType.APPLICATION_JSON).get();

        List<Image> images = response.readEntity(List.class);
        
        assertEquals(1, images.size());
        
        assertEquals("[{id=1, type=gif, product={id=1, name=Gamin 1, description=Clock 1, product=null}}]", images.toString());
    }
    
    @Test
    public void testD1() {
    	List<Product> products = new ArrayList<>();
    	
    	Product product = new Product();
    	product.setId(2);
    	
    	products.add(product);
    	
        Response response = client.target(uri).path("product-manager-app").path("products").path("addChildren").path("1")
                                  .request(MediaType.APPLICATION_JSON)
                                  .post(Entity.entity(products, MediaType.APPLICATION_JSON));

       assertEquals("Created", response.getStatusInfo().getReasonPhrase());
    }
    
    @Test
    public void testD2() {
    	 Client client = ClientBuilder.newClient();
         Response response = client.target(uri).path("product-manager-app").path("products").path("detailsChildren").path("1")
                                   .request(MediaType.APPLICATION_JSON).get();

        List<Product> products = response.readEntity(List.class);
        
        assertEquals(1, products.size());
        
        assertEquals("[{id=2, name=Gamin 2, description=Clock 2, product={id=1, name=Gamin 1, description=Clock 1, product=null}}]", products.toString());
    }
}
