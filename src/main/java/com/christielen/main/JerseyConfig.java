package com.christielen.main;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@ApplicationPath("/product-manager-app")
@Component
public class JerseyConfig extends ResourceConfig{
	public JerseyConfig() {
		packages("com.christielen.service");
	}
}