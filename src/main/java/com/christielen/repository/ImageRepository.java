package com.christielen.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.christielen.domain.Image;
import com.christielen.service.dto.ImageDto;

@Repository
public interface ImageRepository extends CrudRepository<Image, Integer> {

	@Query("select i from Image i where i.product.id = ?1")
	List<Image> findChildrenByParent(Integer productId);

	@Query("select new com.christielen.service.dto.ImageDto(i.id,i.type) from Image i")
	public List<ImageDto> findAllWithOutRelationship();
	
	@Query("select new com.christielen.service.dto.ImageDto(i.id,i.type) from Image i where i.id = ?1")
	public ImageDto findByIdWithOutRelationship(Integer id);
}
