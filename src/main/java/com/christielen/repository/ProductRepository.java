package com.christielen.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.christielen.domain.Product;
import com.christielen.service.dto.ProductDto;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer>  {
	
	public Product findByName(String name);
	
	@Query("select p from Product p where p.product.id = ?1")
	public List<Product> findChildrenByParent(Integer id);
	
	@Query("select new com.christielen.service.dto.ProductDto(p.id,p.name,p.description) from Product p")
	public List<ProductDto> findAllWithOutRelationship();
	
	@Query("select new com.christielen.service.dto.ProductDto(p.id,p.name,p.description) from Product p where p.id = ?1")
	public ProductDto findByIdWithOutRelationship(Integer id);
}
