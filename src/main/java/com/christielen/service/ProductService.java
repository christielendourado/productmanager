package com.christielen.service;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import com.christielen.domain.Product;
import com.christielen.repository.ProductRepository;
import com.christielen.service.dto.ProductDto;

@Path("products")
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(Product product) {
        Product productOld = productRepository.findByName(product.getName());
        if (productOld!=null) {
		    return Response.status(Status.CONFLICT).build();
        }else{
        	productRepository.save(product);
        }
        return Response.created(URI.create("/product-manager-app/products/"+ product.getId())).build();
	}
	
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)	
	public Response update(Product product) {
		Product productOld = productRepository.findOne(product.getId());
        if (productOld!=null) {
        	productOld.setDescription(product.getDescription());
        	productRepository.save(productOld);
        }else{
        	return Response.status(Response.Status.NOT_FOUND).entity("Resource not found for ID: " + product.getId()).build();
        }
        return Response.created(URI.create("/product-manager-app/products/"+ productOld.getId())).build();
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Consumes(MediaType.APPLICATION_JSON)		
	public Response delete(@PathParam("id") Integer id) {
		productRepository.delete(id);
		return Response.noContent().build();
	} 
	
	@GET
	@Path("/details")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(){
		List<Product> products = (List<Product>) productRepository.findAll();		
		return Response.ok(products).build();
	}
	
	@GET
	@Path("/detailsWithoutRelationship")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllWithoutRelationship(){
		 List<ProductDto> products = (List<ProductDto>) productRepository.findAllWithOutRelationship();	
	     return Response.ok(products).build();
		
	}
	
	@GET
	@Path("/detailsChildren/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllChildrenByProductId(@PathParam("id") Integer id){
		List<Product> products = (List<Product>) productRepository.findChildrenByParent(id);	
		return Response.ok(products).build();
		
	}
	
	@GET
	@Path("/detail/{id}")	
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") final Integer id){
		return Response.ok(productRepository.findOne(id)).build();
	}
	
	@GET
	@Path("/detailWithoutRelationship/{id}")	
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByIdWithoutRelationship(@PathParam("id") final Integer id){
		return Response.ok(productRepository.findByIdWithOutRelationship(id)).build();
	}
	
	@POST
	@Path("/addChildren/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addChildrenByProductId(@PathParam("id") Integer id, List<Product> products) {
		Product productParent = productRepository.findOne(id);
		
		if(productParent == null){
			return Response.status(Response.Status.NOT_FOUND).entity("Resource not found for ID: " + id).build();
		}
		
		products.stream().forEach( p -> {
				p = productRepository.findOne(p.getId());
				p.setProduct(productParent);
				productRepository.save(p);
			}
		);
        return Response.created(URI.create("/product-manager-app/detailsChildren/" + id)).build();
	}	
}