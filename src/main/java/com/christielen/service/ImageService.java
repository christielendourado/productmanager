package com.christielen.service;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import com.christielen.domain.Image;
import com.christielen.domain.Product;
import com.christielen.repository.ImageRepository;
import com.christielen.repository.ProductRepository;
import com.christielen.service.dto.ImageDto;

@Path("images")
public class ImageService {
	
	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(Image image) {
        imageRepository.save(image);
        return Response.created(URI.create("/product-manager-app/images/"+ image.getId())).build();
	}
	
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)	
	public Response update(Image image) {
		Image imageOld = imageRepository.findOne(image.getId());
        if (imageOld!=null) {
        	imageOld.setType(image.getType());
        	imageRepository.save(imageOld);
        }else{
        	return Response.status(Response.Status.NOT_FOUND).entity("Resource not found for ID: " + image.getId()).build();
        }
        return Response.created(URI.create("/product-manager-app/images/"+ imageOld.getId())).build();
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Consumes(MediaType.APPLICATION_JSON)		
	public Response delete(@PathParam("id") Integer id) {
		imageRepository.delete(id);
		return Response.noContent().build();
	} 
	
	@GET
	@Path("/details")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(){
		List<Image> images = (List<Image>) imageRepository.findAll();		
		return Response.ok(images).build();		
	}
	
	@GET
	@Path("/detailsWithoutRelationship")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllWithoutRelationshi(){
		List<ImageDto> images = (List<ImageDto>) imageRepository.findAllWithOutRelationship();	
		return Response.ok(images).build();		
	}
	
	@Path("/detail/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") final Integer id){
		return Response.ok(imageRepository.findOne(id)).build();
	}
	
	@Path("/detailWithoutRelationship/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByIdWithoutRelationship(@PathParam("id") final Integer id){
		return Response.ok(imageRepository.findByIdWithOutRelationship(id)).build();
	}
	
	@GET
	@Path("/detailsImages/{productId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllImagesByProductId(@PathParam("productId") Integer productId){
		List<Image> images = (List<Image>) imageRepository.findChildrenByParent(productId);	
		return Response.ok(images).build();
		
	}
	
	@POST
	@Path("/addImage/{productId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addImageByProductId(@PathParam("productId") Integer productId, List<Image> images) {
		Product productParent = productRepository.findOne(productId);
		
		if(productParent == null){
			return Response.status(Response.Status.NOT_FOUND).entity("Resource not found for ID: " + productId).build();
		}
		
		images.stream().forEach( i -> {
				i = imageRepository.findOne(i.getId());
				i.setProduct(productParent);
				imageRepository.save(i);
			}
		);
        return Response.created(URI.create("/product-manager-app/detailsImages/" + productId)).build();
	}
}